/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * The DFL::Impl::SecretsKeyring is an implementation class that provides access to FDO Secrets.
 **/

#include "SecretsKeyring.hpp"

DFL::Impl::SecretsKeyring::SecretsKeyring() {
    qDBusRegisterMetaType<Secret>();
    qDBusRegisterMetaType<SecretsMap>();
    qDBusRegisterMetaType<PropertiesMap>();
    qDBusRegisterMetaType<StrStrMap>();

    mService = new Secrets::Service();

    QDBusObjectPath sessionPath;

    /** We do not have support for encrypted password exchange */
    QDBusReply<QDBusVariant> retVal = mService->OpenSession( "plain", QDBusVariant( "" ), sessionPath );

    if ( (sessionPath.path().length() <= 0) or (retVal.isValid() == false) ) {
        qCritical() << "Failed to create session";
        qCritical() << "Received:";
        qCritical() << "    " << retVal.error().message();

        mError       = DFL::Keyring::OtherError;
        mErrorString = QString( "Unable to open Session: " + retVal.error().message() );

        return;
    }

    /** Prepare the session */
    mSession = new Secrets::Session( sessionPath.path() );

    if ( mSession->isValid() ) {
        mValid = true;
    }
}


QByteArray DFL::Impl::SecretsKeyring::retrieveEntry( QString key, QString wallet ) {
    if ( mValid == false ) {
        return QByteArray();
    }

    mError = DFL::Keyring::NoError;

    /** Try to get the default wallet */
    if ( wallet.length() == 0 ) {
        wallet = getDefaultWallet();

        if ( wallet.length() == 0 ) {
            mError = DFL::Keyring::WalletNotFound;
            return QByteArray();
        }
    }

    /** If this wallet is not known, update the list of wallets */
    if ( mCollections.contains( wallet ) == false ) {
        /** Unknown wallet */
        if ( wallets().contains( wallet ) == false ) {
            mError = DFL::Keyring::WalletNotFound;
            return QByteArray();
        }
    }

    /** Open the wallet for transactions */
    if ( open( wallet ) == false ) {
        return QByteArray();
    }

    /** Wallet exists. If the key is not knwon, update the keys */
    if ( mKeys[ wallet ].contains( mApplication + "/" + key ) == false ) {
        /** Unknown key */
        if ( keys( wallet ).contains( mApplication + "/" + key ) == false ) {
            mError = DFL::Keyring::EntryNotFound;
            return QByteArray();
        }
    }

    Secrets::Item *item = mKeys[ wallet ][ mApplication + "/" + key ];

    /** The item is locked */
    if ( item->locked() ) {
        /** Attempt to unlock it */
        if ( unlock( { QDBusObjectPath( item->path() ) } ) == false ) {
            /** unlock(...) sets the error when returning false */
            return QByteArray();
        }
    }

    QDBusReply<Secret> secRet = item->GetSecret( QDBusObjectPath( mSession->path() ) );

    if ( secRet.isValid() ) {
        Secret password = secRet.value();
        return password.value;
    }

    /** Something went wrong */
    mError       = DFL::Keyring::OtherError;
    mErrorString = QString( secRet.error().message() );

    return QByteArray();
}


bool DFL::Impl::SecretsKeyring::createEntry( QString key, QByteArray password, QString wallet, bool overwrite ) {
    if ( mValid == false ) {
        return false;
    }

    mError = DFL::Keyring::NoError;

    /** Try to get the default wallet */
    if ( wallet.length() == 0 ) {
        wallet = getDefaultWallet();

        if ( wallet.length() == 0 ) {
            mError = DFL::Keyring::WalletNotFound;
            return false;
        }
    }

    /** If this wallet is not known, update the list of wallets */
    if ( mCollections.contains( wallet ) == false ) {
        /** Unknown wallet */
        if ( wallets().contains( wallet ) == false ) {
            mError = DFL::Keyring::WalletNotFound;
            return false;
        }
    }

    /** Open the wallet for transactions */
    if ( open( wallet ) == false ) {
        return false;
    }

    PropertiesMap props;
    props.map[ "org.freedesktop.Secret.Item.Label" ]      = mApplication + "/" + key;
    props.map[ "org.freedesktop.Secret.Item.Attributes" ] = QVariant::fromValue( StrStrMap( { { "user", key }, { "server", mApplication } } ) );

    Secret secret;
    secret.session    = QDBusObjectPath( mSession->path() );
    secret.parameters = QByteArray();
    secret.value      = password;
    secret.mimeType   = "text/plain";

    QDBusObjectPath             promptPath;
    QDBusReply<QDBusObjectPath> itemPathRet = mCollections[ wallet ]->CreateItem( props, secret, overwrite, promptPath );

    QString newItemPath;

    if ( itemPathRet.isValid() ) {
        if ( itemPathRet.value().path() != "/" ) {
            newItemPath = itemPathRet.value().path();
        }

        else if ( (itemPathRet.value().path() == "/") and (promptPath.path() != "/") ) {
            bool dismissed = false;
            QList<QDBusObjectPath> reply = executePrompt( promptPath, dismissed );

            if ( dismissed ) {
                mError = DFL::Keyring::AccessDeniedByUser;
                return false;
            }

            /** Successfully: Get the path */
            if ( reply.count() ) {
                newItemPath = reply.at( 0 ).path();
            }
        }

        if ( newItemPath.length() ) {
            Secrets::Item *item = new Secrets::Item( newItemPath );

            if ( item->isValid() ) {
                mKeys[ wallet ][ item->label() ] = item;
            }

            return true;
        }
    }

    mError       = DFL::Keyring::OtherError;
    mErrorString = itemPathRet.error().message();

    return false;
}


bool DFL::Impl::SecretsKeyring::deleteEntry( QString key, QString wallet ) {
    if ( mValid == false ) {
        return false;
    }

    mError = DFL::Keyring::NoError;

    /** Try to get the default wallet */
    if ( wallet.length() == 0 ) {
        wallet = getDefaultWallet();

        if ( wallet.length() == 0 ) {
            mError = DFL::Keyring::WalletNotFound;
            return false;
        }
    }

    /** If this wallet is not known, update the list of wallets */
    if ( mCollections.contains( wallet ) == false ) {
        /** Unknown wallet */
        if ( wallets().contains( wallet ) == false ) {
            mError = DFL::Keyring::WalletNotFound;
            return false;
        }
    }

    /** Wallet exists. If the key is not knwon, update the keys */
    if ( mKeys[ wallet ].contains( mApplication + "/" + key ) == false ) {
        /** Unknown key */
        if ( keys( wallet ).contains( mApplication + "/" + key ) == false ) {
            mError = DFL::Keyring::EntryNotFound;
            return false;
        }
    }

    /** Open the wallet for transactions */
    if ( open( wallet ) == false ) {
        return false;
    }

    Secrets::Item               *item     = mKeys[ wallet ][ mApplication + "/" + key ];
    QDBusReply<QDBusObjectPath> promptRet = item->Delete();

    if ( promptRet.isValid() ) {
        QDBusObjectPath prompt = promptRet.value();

        /** Deleted */
        if ( prompt.path() == "/" ) {
            return true;
        }

        bool dismissed = false;
        QList<QDBusObjectPath> reply = executePrompt( prompt, dismissed );

        if ( dismissed ) {
            mError = DFL::Keyring::AccessDeniedByUser;
            return false;
        }

        /** Successfully deleted */
        if ( reply.length() != 0 ) {
            return true;
        }
    }

    mError       = DFL::Keyring::OtherError;
    mErrorString = QString( promptRet.error().message() );

    return false;
}


bool DFL::Impl::SecretsKeyring::changeWalletPassword( QString ) {
    mError = DFL::Keyring::NotImplemented;
    return false;
}


bool DFL::Impl::SecretsKeyring::isOpen( QString wallet ) {
    if ( mValid == false ) {
        return false;
    }

    mError = DFL::Keyring::NoError;

    /** If this wallet is not known, update the list */
    if ( mCollections.contains( wallet ) == false ) {
        /** Unknown wallet */
        if ( wallets().contains( wallet ) == false ) {
            mError = DFL::Keyring::WalletNotFound;
            return false;
        }
    }

    return (mCollections[ wallet ]->locked() == false);
}


QStringList DFL::Impl::SecretsKeyring::wallets() {
    if ( mValid == false ) {
        return QStringList();
    }

    mError = DFL::Keyring::NoError;

    QStringList            walletList;
    QList<QDBusObjectPath> collectionsObjs = mService->collections();
    for ( QDBusObjectPath objPath: collectionsObjs ) {
        Secrets::Collection *coll = new Secrets::Collection( objPath.path() );

        if ( coll->isValid() ) {
            walletList << coll->label();

            if ( mCollections.contains( coll->label() ) == false ) {
                mCollections[ coll->label() ] = coll;
            }
        }
    }

    return walletList;
}


QStringList DFL::Impl::SecretsKeyring::keys( QString wallet ) {
    if ( mValid == false ) {
        return QStringList();
    }

    mError = DFL::Keyring::NoError;

    /** Try to get the default wallet */
    if ( wallet.length() == 0 ) {
        wallet = getDefaultWallet();

        if ( wallet.length() == 0 ) {
            mError = DFL::Keyring::WalletNotFound;
            return QStringList();
        }
    }

    /** If this wallet is not known, update the list */
    if ( mCollections.contains( wallet ) == false ) {
        /** Unknown wallet */
        if ( wallets().contains( wallet ) == false ) {
            mError = DFL::Keyring::WalletNotFound;
            return QStringList();
        }
    }

    QList<QDBusObjectPath>         itemPaths = mCollections[ wallet ]->items();
    QMap<QString, Secrets::Item *> items;
    for ( QDBusObjectPath itemPath: itemPaths ) {
        Secrets::Item *item = new Secrets::Item( itemPath.path() );

        if ( item->isValid() ) {
            items[ item->label() ] = item;
        }
    }

    mKeys[ wallet ] = items;

    return mKeys[ wallet ].keys();
}


bool DFL::Impl::SecretsKeyring::open( QString wallet ) {
    if ( mValid == false ) {
        return false;
    }

    mError = DFL::Keyring::NoError;

    /** If this wallet is not known, update the list of wallets */
    if ( mCollections.contains( wallet ) == false ) {
        /** Unknown wallet */
        if ( wallets().contains( wallet ) == false ) {
            mError = DFL::Keyring::WalletNotFound;
            return false;
        }
    }

    /** If it's not locked, then it's open */
    if ( mCollections[ wallet ]->locked() == false ) {
        return true;
    }

    /** Attempt to Unlock it */
    return unlock( { QDBusObjectPath( mCollections[ wallet ]->path() ) } );
}


bool DFL::Impl::SecretsKeyring::unlock( QList<QDBusObjectPath> objPaths ) {
    QDBusObjectPath        promptPath;
    QList<QDBusObjectPath> result;

    QDBusReply<QList<QDBusObjectPath> > ret = mService->Unlock( objPaths, promptPath );

    if ( ret.isValid() ) {
        result << ret.value();
    }

    else {
        mError       = DFL::Keyring::OtherError;
        mErrorString = QString( ret.error().message() );

        return false;
    }

    if ( promptPath.path() != "/" ) {
        bool dismissed = false;
        QList<QDBusObjectPath> promptResult = executePrompt( promptPath, dismissed );

        if ( dismissed ) {
            mError = DFL::Keyring::AccessDeniedByUser;
            return false;
        }

        for ( QDBusObjectPath opath: promptResult ) {
            result << opath;
        }
    }

    /** Either not prompted, or prompt was executed */
    for ( QDBusObjectPath o: objPaths ) {
        if ( result.contains( o ) == false ) {
            /** Access was denied due to other reasons */
            mError = DFL::Keyring::AccessDenied;
            return false;
        }
    }

    mError = DFL::Keyring::NoError;
    return true;
}


QList<QDBusObjectPath> DFL::Impl::SecretsKeyring::executePrompt( QDBusObjectPath promptPath, bool& dismissed ) {
    QList<QDBusObjectPath> result;

    Secrets::Prompt *prompt = new Secrets::Prompt( promptPath.path() );

    prompt->doPrompt( "" );
    prompt->waitForCompleted( -1 );

    dismissed = prompt->wasDismissed();

    for ( QDBusObjectPath opath: prompt->result() ) {
        result << opath;
    }

    return result;
}


bool DFL::Impl::SecretsKeyring::isAvailable() {
    QDBusInterface iface( "org.freedesktop.secrets", "/org/freedesktop/secrets", "org.freedesktop.Secret", QDBusConnection::sessionBus() );

    return iface.isValid();
}


QString DFL::Impl::SecretsKeyring::getDefaultWallet() {
    QDBusInterface iface(
        "org.freedesktop.secrets",
        "/org/freedesktop/secrets/aliases/default",
        "org.freedesktop.Secret.Collection",
        QDBusConnection::sessionBus()
    );

    if ( iface.isValid() ) {
        return iface.property( "Label" ).toString();
    }

    return QString();
}
