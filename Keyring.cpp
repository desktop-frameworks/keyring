/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * The DFL::Keyring provides a simple and easy to use implementation of Keyring.
 * This class is built to provide access KWallet/GnomeKeyring/SecretsAPI/LXQtWallet/DesQWallet.
 * No backend specific code will be required.
 **/

#include "DFKeyring.hpp"
#include "DummyKeyring.hpp"

#include "KWalletKeyring.hpp"
#include "SecretsKeyring.hpp"
#include "LXQtKeyring.hpp"
#include "InbuiltKeyring.hpp"

DFL::Keyring * DFL::Keyring::createInstance( DFL::Keyring::Backend backend, QString application ) {
    DFL::Keyring *instance = nullptr;

    switch ( backend ) {
        case DFL::Keyring::SecretsAPI: {
            instance               = new DFL::Impl::SecretsKeyring();
            instance->mBackend     = DFL::Keyring::SecretsAPI;
            instance->mApplication = application;

            break;
        }

        case DFL::Keyring::Gnome: {
            qWarning() << "Note that GnomeKeyring backend uses Secrets API.";
            instance               = new DFL::Impl::SecretsKeyring();
            instance->mBackend     = DFL::Keyring::SecretsAPI;
            instance->mApplication = application;

            break;
        }

        case DFL::Keyring::KWallet4: {
            instance               = new DFL::Impl::KWalletKeyring( 4 );
            instance->mBackend     = DFL::Keyring::KWallet4;
            instance->mApplication = application;

            break;
        }

        case DFL::Keyring::KWallet5: {
            instance               = new DFL::Impl::KWalletKeyring( 5 );
            instance->mBackend     = DFL::Keyring::KWallet5;
            instance->mApplication = application;

            break;
        }

        case DFL::Keyring::KWallet6: {
            instance               = new DFL::Impl::KWalletKeyring( 6 );
            instance->mBackend     = DFL::Keyring::KWallet6;
            instance->mApplication = application;

            break;
        }

        case DFL::Keyring::LXQt: {
            instance               = new DFL::Impl::DummyKeyring();
            instance->mBackend     = DFL::Keyring::Dummy;
            instance->mApplication = application;
            instance->mError       = DFL::Keyring::NotImplemented;

            break;
        }

        case DFL::Keyring::Inbuilt: {
            instance               = new DFL::Impl::DummyKeyring();
            instance->mBackend     = DFL::Keyring::Dummy;
            instance->mApplication = application;
            instance->mError       = DFL::Keyring::NotImplemented;

            break;
        }

        case DFL::Keyring::Dummy: {
            instance               = new DFL::Impl::DummyKeyring();
            instance->mBackend     = DFL::Keyring::Dummy;
            instance->mApplication = application;

            break;
        }
    }

    if ( instance == nullptr ) {
        instance               = new DFL::Impl::DummyKeyring();
        instance->mBackend     = DFL::Keyring::Dummy;
        instance->mApplication = application;
        instance->mError       = DFL::Keyring::OtherError;
    }

    return instance;
}


DFL::Keyring::Backend DFL::Keyring::backend() {
    return mBackend;
}


DFL::Keyring::Error DFL::Keyring::error() {
    return mError;
}


QString DFL::Keyring::errorString() {
    switch ( mError ) {
        case DFL::Keyring::NoError: {
            return QString();
        }

        case DFL::Keyring::EntryNotFound: {
            return QString( "The requested entry was not found in the wallet." );
        }

        case DFL::Keyring::WalletNotFound: {
            return QString( "The reuqested wallet was not found." );
        }

        case DFL::Keyring::AccessDeniedByUser: {
            return QString( "User did not provide access to the given wallet." );
        }

        case DFL::Keyring::AccessDenied: {
            return QString( "Unable to access the wallet." );
        }

        case DFL::Keyring::BackendNotAvailable: {
            return QString( "The requested backend is not available on this system." );
        }

        case DFL::Keyring::NotImplemented: {
            return QString( "This feature is not implemented in the current backend." );
        }

        case DFL::Keyring::OtherError: {
            if ( mErrorString.length() ) {
                return mErrorString;
            }

            return QString( "An unknwon error has occurred." );
        }
    }

    return QString();
}


DFL::Keyring::Backends DFL::Keyring::availableBackends() {
    DFL::Keyring::Backends list;

    for ( int i = 0; i <= DFL::Keyring::Backend::Dummy; i++ ) {
        DFL::Keyring::Backend backend = (DFL::Keyring::Backend)i;
        switch ( backend ) {
            case DFL::Keyring::SecretsAPI: {
                if ( DFL::Impl::SecretsKeyring::isAvailable() ) {
                    list << DFL::Keyring::SecretsAPI;
                }

                break;
            }

            case DFL::Keyring::Gnome: {
                if ( DFL::Impl::SecretsKeyring::isAvailable() ) {
                    list << DFL::Keyring::Gnome;
                }

                break;
            }

            case DFL::Keyring::KWallet4: {
                if ( DFL::Impl::KWalletKeyring::isAvailable( 4 ) ) {
                    list << DFL::Keyring::KWallet4;
                }

                break;
            }

            case DFL::Keyring::KWallet5: {
                if ( DFL::Impl::KWalletKeyring::isAvailable( 5 ) ) {
                    list << DFL::Keyring::KWallet5;
                }

                break;
            }

            case DFL::Keyring::KWallet6: {
                if ( DFL::Impl::KWalletKeyring::isAvailable( 6 ) ) {
                    list << DFL::Keyring::KWallet6;
                }

                break;
            }

            case DFL::Keyring::LXQt: {
                if ( DFL::Impl::LXQtKeyring::isAvailable() ) {
                    list << DFL::Keyring::LXQt;
                }

                break;
            }

            case DFL::Keyring::Inbuilt: {
                if ( DFL::Impl::InbuiltKeyring::isAvailable() ) {
                    list << DFL::Keyring::Inbuilt;
                }

                break;
            }

            case DFL::Keyring::Dummy: {
                if ( DFL::Impl::DummyKeyring::isAvailable() ) {
                    list << DFL::Keyring::Dummy;
                }

                break;
            }
        }
    }

    return list;
}
