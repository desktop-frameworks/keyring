/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * The file contains implementation for various DBus types required by FDO Secrets API.
 **/

#pragma once

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>

#define SecretsService    "org.freedesktop.secrets"
#define SecretsObject     "/org/freedesktop/secrets"

namespace Secrets {
    class Collection;
    class Item;
    class Prompt;
    class Service;
    class Session;
}

class Secret {
    public:
        Secret() = default;

        Secret( QDBusObjectPath iSession, const QByteArray& iValue, QString iMimeType ) : session( std::move( iSession ) )
            , value( iValue )
            , mimeType( std::move( iMimeType ) ) {
        }

        friend QDBusArgument &operator<<( QDBusArgument& arg, const Secret& secret );
        friend const QDBusArgument &operator>>( const QDBusArgument& arg, Secret& secret );

        QDBusObjectPath session;
        QByteArray parameters;
        QByteArray value;
        QString mimeType;
};

struct PropertiesMap {
    QVariantMap map;
};

typedef QMap<QDBusObjectPath, Secret>   SecretsMap;
typedef QMap<QString, QString>          StrStrMap;

QDataStream &operator<<( QDataStream& stream, const QDBusObjectPath& value );
QDataStream &operator>>( QDataStream& stream, QDBusObjectPath& value );

const QDBusArgument &operator>>( const QDBusArgument& arg, PropertiesMap& value );
QDBusArgument &operator<<( QDBusArgument& arg, const PropertiesMap& value );


Q_DECLARE_METATYPE( Secret )
Q_DECLARE_METATYPE( SecretsMap )
Q_DECLARE_METATYPE( PropertiesMap )
Q_DECLARE_METATYPE( StrStrMap )
