/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * The DFL::Impl::SecretsKeyring is an implementation class that provides access to FDO Secrets.
 **/

#pragma once

#include "DFKeyring.hpp"

#include "Secrets/Collection.hpp"
#include "Secrets/Item.hpp"
#include "Secrets/Prompt.hpp"
#include "Secrets/Service.hpp"
#include "Secrets/Session.hpp"
#include "Secrets/Types.hpp"

namespace DFL {
    namespace Impl {
        class SecretsKeyring;
    }
}

class DFL::Impl::SecretsKeyring : public DFL::Keyring {
    Q_OBJECT;

    public:
        SecretsKeyring();

        /**
         * Read the password for a key in a given wallet.
         * @v key    - Key to be retrieved
         * @v wallet - Wallet to be searched
         *
         * @return   - Password or empty QByteArray if key is not found
         */
        QByteArray retrieveEntry( QString key, QString wallet ) override;

        /**
         * Store the password for a key in a given wallet.
         * If the key already exists in a given wallet, it may be over-written.
         */
        bool createEntry( QString key, QByteArray value, QString wallet, bool overwrite ) override;

        /**
         * Delete the key and the corresponding password from the given wallet.
         * If the key is not found, nothing is is done.
         */
        bool deleteEntry( QString, QString ) override;

        /**
         * Change the wallet password.
         * This may open a dedicated GUI.
         */
        bool changeWalletPassword( QString ) override;

        /**
         * Check if the given wallet is open.
         * Will return false if the wallet does not exist.
         */
        bool isOpen( QString ) override;

        /**
         * Check if this backend is available
         * Will return false if the wallet does not exist.
         */
        static bool isAvailable();

        /**
         * List all the available wallets in the given backend.
         * Will return 'Collections' ofr fdo-secrets, wallets for KWallet.
         */
        QStringList wallets() override;

        /** List all the keys of a given wallet */
        QStringList keys( QString ) override;

    private:

        /**
         * Helper functions
         * @f open - Attempts to open the wallet.
         * @f unlock - Calls Secrets::Service::Unlock(...)
         * @f executePrompt - Executes a prompt.
         */
        bool open( QString wallet );
        bool unlock( QList<QDBusObjectPath> objPaths );
        QList<QDBusObjectPath> executePrompt( QDBusObjectPath promptPath, bool& dismissed );

        QString getDefaultWallet();

        Secrets::Service *mService = nullptr;
        Secrets::Session *mSession = nullptr;

        QMap<QString, Secrets::Collection *> mCollections;
        QMap<QString, QMap<QString, Secrets::Item *> > mKeys;

        bool mValid = false;
};
