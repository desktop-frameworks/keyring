/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * The DFL::Impl::InbuiltKeyring is an implementation class that provides access to our inbuilt Keyring.
 **/
