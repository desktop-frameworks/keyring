# DFL::Keyring

This library provides a simple and easy to use implementation of Keyring. Apart from the Inbuilt Key Store, this library allows you to use
KWallet, LXQt Keyring, and Gnome Keyring backends. Additionally, Secrets API implementation is also available.


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/keyring.git DFLKeyring`
- Enter the `DFLKeyring` folder
  * `cd DFLKeyring`
- Configure the project - we use meson for project management
  * `meson setup .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Dependencies:
* Qt5/Qt6 (qtbase5-dev, qtbase5-dev-tools)
* gnome-keyring (runtine, optional)
* kwalletd (runtine, optional)


### Known Bugs
* Inbuilt Key Store does nothing.
* LXQt Key Store does nothing.


### Upcoming
* Any other feature you request for... :)
