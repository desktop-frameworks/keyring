/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * The DFL::Keyring provides a simple and easy to use implementation of Keyring.
 **/

#pragma once

#include <QtCore>

namespace DFL {
    class Keyring;
}

class DFL::Keyring : public QObject {
    Q_OBJECT;

    public:
        enum Error {
            NoError,             /** No error occurred, operation was successful */
            EntryNotFound,       /** For the given key no data was found */
            WalletNotFound,      /** The given wallet does not exist */
            AccessDeniedByUser,  /** User denied access to keychain */
            AccessDenied,        /** Access denied for other reasons */
            BackendNotAvailable, /** This backend was not available */
            NotImplemented,      /** Not implemented on platform/backend */
            OtherError           /** Something else went wrong (errorString() might provide details) */
        };

        enum Backend {
            SecretsAPI,     /** FDO Secrets Service API (via DBus) */
            Gnome,          /** Gnome Keyring backend (via QLibrary) */
            KWallet4,       /** KWallet4 from KDE4 (via DBus) */
            KWallet5,       /** KWallet5 from KDE5 (via DBus) */
            KWallet6,       /** KWallet6 from KDE6 (via DBus) */
            LXQt,           /** Not supported at the moment */
            Inbuilt,        /** Not supported at the moment */
            Dummy,          /** Dummy backend - it does nothing */
        };

        typedef QList<Backend> Backends;

        /**
         * Create an instance of DFL::Keyring class.
         * @v backend: The backend to use.
         * @v application: Will be used to create folders/collections.
         *
         * This function will always return a valid pointer. If the given
         * backend is not available at the given time, or any error occurs,
         * a dummy instance will be returned with with a suitable error
         * enum set.
         *
         * Note: User is responsible for deleting the returned pointer.
         */
        static DFL::Keyring * createInstance( DFL::Keyring::Backend backend, QString application );

        /**
         * Returns the currently available backends.
         * Note that the return values can vary from time-to-time
         * depending on the availability of the backends.
         */
        static DFL::Keyring::Backends availableBackends();

        /**
         * Returns the current backend.
         */
        DFL::Keyring::Backend backend();

        /**
         * Returns the current error.
         */
        DFL::Keyring::Error error();

        /**
         * Returns the current error as a string.
         * Returns empty string if there is no error.
         */
        QString errorString();

        /**
         * Read the password for a key in a given wallet.
         * If the key is not found an empty QByteArray will be returned.
         */
        virtual QByteArray retrieveEntry( QString key, QString wallet ) = 0;

        /**
         * Store the password for a key in a given wallet.
         * If the key already exists in a given wallet, it may be over-written.
         */
        virtual bool createEntry( QString key, QByteArray password, QString wallet, bool overwrite ) = 0;

        /**
         * Delete the key and the corresponding password from the given wallet.
         * If the key is not found, nothing is is done.
         */
        virtual bool deleteEntry( QString key, QString wallet ) = 0;

        /**
         * Change the wallet password.
         * This may open a dedicated GUI.
         */
        virtual bool changeWalletPassword( QString wallet ) = 0;

        /**
         * Check if the given wallet is open.
         * Will return false if the wallet does not exist.
         */
        virtual bool isOpen( QString wallet ) = 0;

        /**
         * List all the available wallets in the given backend.
         * Will return 'Collections' ofr fdo-secrets, wallets for KWallet.
         */
        virtual QStringList wallets() = 0;

        /** List all the keys of a given wallet */
        virtual QStringList keys( QString wallet ) = 0;

        /** Destructor */
        ~Keyring() = default;

    protected:
        DFL::Keyring::Backend mBackend;
        DFL::Keyring::Error mError = NoError;
        QString mErrorString;

        QString mApplication;

        /**
         * This is our internal initializer.
         * Just stores the backend and application
         * Use DFL::Keyring::createInstance(...) to obtain an instance
         */
        Keyring() = default;
};
