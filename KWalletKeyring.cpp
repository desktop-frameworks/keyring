/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * The DFL::Impl::KWalletKeyring is an implementation class that provides access to KWallet 4/5/6.
 **/

#include "KWalletKeyring.hpp"

DFL::Impl::KWalletKeyring::KWalletKeyring( int version ) {
    QString service;
    QString path;

    if ( version == 4 ) {
        service = "org.kde.kwalletd";
        path    = "/modules/kwalletd";
    }

    else if ( version == 5 ) {
        service = "org.kde.kwalletd5";
        path    = "/modules/kwalletd5";
    }

    else if ( version == 6 ) {
        service = "org.kde.kwalletd6";
        path    = "/modules/kwalletd6";
    }

    else {
        mError       = DFL::Keyring::OtherError;
        mErrorString = QString( "Unknown KWallet version. Choose between 4, 5 or 6." );

        return;
    }

    kwIface = new QDBusInterface( service, path, "org.kde.KWallet", QDBusConnection::sessionBus() );

    if ( kwIface->isValid() ) {
        mValid = true;
    }

    else {
        mErrorString = QString( kwIface->lastError().message() );
    }
}


QByteArray DFL::Impl::KWalletKeyring::retrieveEntry( QString key, QString wallet ) {
    if ( mValid == false ) {
        return QByteArray();
    }

    mError = DFL::Keyring::NoError;

    /** Try to get the default wallet */
    if ( wallet.length() == 0 ) {
        wallet = getDefaultWallet();

        if ( wallet.length() == 0 ) {
            mError = DFL::Keyring::WalletNotFound;
            return QByteArray();
        }
    }

    if ( wallets().contains( wallet ) == false ) {
        mError = DFL::Keyring::WalletNotFound;
        return QByteArray();
    }

    /** Open the wallet for transactions */
    if ( open( wallet ) == false ) {
        return QByteArray();
    }

    QString folder = mApplication;

    /** Get the keys */
    keys( wallet );

    if ( mKeysMap[ wallet ].contains( folder ) == false ) {
        mError = DFL::Keyring::EntryNotFound;
        return QByteArray();
    }

    if ( mKeysMap[ wallet ][ folder ].contains( key ) == false ) {
        mError = DFL::Keyring::EntryNotFound;
        return QByteArray();
    }

    QDBusReply<QString> readRet = kwIface->call( "readPassword", walletHandleMap[ wallet ], folder, key, mApplication );

    if ( readRet.isValid() ) {
        return readRet.value().toUtf8();
    }

    mError       = DFL::Keyring::OtherError;
    mErrorString = readRet.error().message();

    return QByteArray();
}


bool DFL::Impl::KWalletKeyring::createEntry( QString key, QByteArray password, QString wallet, bool overwrite ) {
    if ( mValid == false ) {
        return false;
    }

    mError = DFL::Keyring::NoError;

    /** Try to get the default wallet */
    if ( wallet.length() == 0 ) {
        wallet = getDefaultWallet();

        if ( wallet.length() == 0 ) {
            mError = DFL::Keyring::WalletNotFound;
            return false;
        }
    }

    if ( wallets().contains( wallet ) == false ) {
        mError = DFL::Keyring::WalletNotFound;
        return false;
    }

    /** Open the wallet for transactions */
    if ( open( wallet ) == false ) {
        return false;
    }

    QString folder = mApplication;

    /** Get the keys */
    keys( wallet );

    if ( mKeysMap[ wallet ].contains( folder ) == false ) {
        QDBusReply<int> folderRet = kwIface->call( "createFolder", walletHandleMap[ wallet ], folder, mApplication );

        if ( folderRet.isValid() ) {
            if ( folderRet.value() != 0 ) {
                mError       = DFL::Keyring::OtherError;
                mErrorString = QString( "Failed to create folder: " + folder );

                return false;
            }
        }
    }

    if ( overwrite == false ) {
        if ( mKeysMap[ wallet ][ folder ].contains( key ) == true ) {
            mError       = DFL::Keyring::OtherError;
            mErrorString = QString( "Not overwriting existing entry. Use overwrite = true to replace the existing entry." );

            return false;
        }
    }

    QDBusReply<int> writeRet = kwIface->call( "writePassword", walletHandleMap[ wallet ], folder, key, QString( password ), mApplication );

    if ( writeRet.isValid() ) {
        if ( writeRet.value() == 0 ) {
            return true;
        }
    }

    mError       = DFL::Keyring::OtherError;
    mErrorString = QString( writeRet.error().message() );

    return false;
}


bool DFL::Impl::KWalletKeyring::deleteEntry( QString key, QString wallet ) {
    if ( mValid == false ) {
        return false;
    }

    mError = DFL::Keyring::NoError;

    /** Try to get the default wallet */
    if ( wallet.length() == 0 ) {
        wallet = getDefaultWallet();

        if ( wallet.length() == 0 ) {
            mError = DFL::Keyring::WalletNotFound;
            return false;
        }
    }

    if ( wallets().contains( wallet ) == false ) {
        mError = DFL::Keyring::WalletNotFound;
        return false;
    }

    /** Open the wallet for transactions */
    if ( open( wallet ) == false ) {
        return false;
    }

    QString folder = mApplication;

    /** Get the keys */
    keys( wallet );

    if ( mKeysMap[ wallet ].contains( folder ) == false ) {
        mError = DFL::Keyring::EntryNotFound;
        return false;
    }

    if ( mKeysMap[ wallet ][ folder ].contains( key ) == false ) {
        mError = DFL::Keyring::EntryNotFound;
        return false;
    }

    QDBusReply<int> deleteRet = kwIface->call( "removeEntry", walletHandleMap[ wallet ], folder, key, mApplication );

    if ( deleteRet.isValid() ) {
        if ( deleteRet.value() == 0 ) {
            return true;
        }
    }

    mError       = DFL::Keyring::OtherError;
    mErrorString = QString( deleteRet.error().message() );

    return false;
}


bool DFL::Impl::KWalletKeyring::changeWalletPassword( QString wallet ) {
    if ( mValid == false ) {
        return false;
    }

    mError = DFL::Keyring::NoError;

    /** Try to get the default wallet */
    if ( wallet.length() == 0 ) {
        wallet = getDefaultWallet();

        if ( wallet.length() == 0 ) {
            mError = DFL::Keyring::WalletNotFound;
            return false;
        }
    }

    if ( wallets().contains( wallet ) == false ) {
        mError = DFL::Keyring::WalletNotFound;
        return false;
    }

    kwIface->call( "changePassword", wallet, (qlonglong)0, mApplication );

    return true;
}


bool DFL::Impl::KWalletKeyring::isOpen( QString wallet ) {
    if ( mValid == false ) {
        return false;
    }

    mError = DFL::Keyring::NoError;

    /** Try to get the default wallet */
    if ( wallet.length() == 0 ) {
        wallet = getDefaultWallet();

        if ( wallet.length() == 0 ) {
            mError = DFL::Keyring::WalletNotFound;
            return false;
        }
    }

    if ( wallets().contains( wallet ) == false ) {
        mError = DFL::Keyring::WalletNotFound;
        return false;
    }

    return walletHandleMap.contains( wallet );
}


QStringList DFL::Impl::KWalletKeyring::wallets() {
    if ( mValid == false ) {
        return QStringList();
    }

    mError = DFL::Keyring::NoError;

    QDBusReply<QStringList> walletsReply = kwIface->call( "wallets" );

    if ( walletsReply.isValid() ) {
        return walletsReply.value();
    }

    mError       = DFL::Keyring::OtherError;
    mErrorString = walletsReply.error().message();

    return QStringList();
}


QStringList DFL::Impl::KWalletKeyring::keys( QString wallet ) {
    if ( mValid == false ) {
        return QStringList();
    }

    mError = DFL::Keyring::NoError;

    /** Try to get the default wallet */
    if ( wallet.length() == 0 ) {
        wallet = getDefaultWallet();

        if ( wallet.length() == 0 ) {
            mError = DFL::Keyring::WalletNotFound;
            return QStringList();
        }
    }

    /** Cehck if the wallet exists */
    if ( wallets().contains( wallet ) == false ) {
        mError = DFL::Keyring::WalletNotFound;
        return QStringList();
    }

    /** Check if the wallet is open */
    if ( walletHandleMap.contains( wallet ) == false ) {
        if ( open( wallet ) == false ) {
            mError = DFL::Keyring::AccessDenied;
            return QStringList();
        }
    }

    QDBusReply<QStringList> folderRet = kwIface->call( "folderList", walletHandleMap[ wallet ], mApplication );
    QStringList             folders;

    if ( folderRet.isValid() == false ) {
        mError       = DFL::Keyring::OtherError;
        mErrorString = QString( "Unable to retrieve the folder list." );

        return QStringList();
    }

    folders = folderRet.value();

    if ( mKeysMap.contains( wallet ) == false ) {
        mKeysMap[ wallet ] = QMap<QString, QStringList>();
    }

    QStringList allKeys;
    for ( QString folder: folders ) {
        QDBusReply<QStringList> keysRet = kwIface->call( "entryList", walletHandleMap[ wallet ], folder, mApplication );

        if ( keysRet.isValid() ) {
            mKeysMap[ wallet ][ folder ] = keysRet.value();
            QStringList newKeys = keysRet.value().join( "\n" + folder + "/" ).split( "\n", Qt::SkipEmptyParts );

            if ( newKeys.length() > 0 ) {
                newKeys[ 0 ] = folder + "/" + newKeys[ 0 ];
            }

            allKeys << newKeys;
        }
    }

    return allKeys;
}


bool DFL::Impl::KWalletKeyring::open( QString wallet ) {
    if ( mValid == false ) {
        return false;
    }

    mError = DFL::Keyring::NoError;

    /** Check if this wallet exists */
    if ( wallets().contains( wallet ) == false ) {
        mError = DFL::Keyring::WalletNotFound;
        return false;
    }

    QDBusReply<int> handleRet = kwIface->call( "open", wallet, (qlonglong)0, mApplication );

    if ( handleRet.isValid() ) {
        int handle = handleRet.value();
        walletHandleMap[ wallet ] = handle;

        return true;
    }

    mError       = DFL::Keyring::OtherError;
    mErrorString = handleRet.error().message();

    return false;
}


bool DFL::Impl::KWalletKeyring::isAvailable( int version ) {
    QString service;
    QString path;

    if ( version == 4 ) {
        service = "org.kde.kwalletd";
        path    = "/modules/kwalletd";
    }

    else if ( version == 5 ) {
        service = "org.kde.kwalletd5";
        path    = "/modules/kwalletd5";
    }

    else if ( version == 4 ) {
        service = "org.kde.kwalletd6";
        path    = "/modules/kwalletd6";
    }

    else {
        return false;
    }

    QDBusInterface iface( service, path, "org.kde.KWallet", QDBusConnection::sessionBus() );

    return iface.isValid();
}


QString DFL::Impl::KWalletKeyring::getDefaultWallet() {
    if ( mValid == false ) {
        return QString();
    }

    QDBusReply<QString> replyRet = kwIface->call( "localWallet" );

    if ( replyRet.isValid() ) {
        return replyRet.value();
    }

    else {
        mErrorString = replyRet.error().message();
    }

    return QString();
}
