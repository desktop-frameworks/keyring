/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * The Secrets::Service is an implementation class that provides access to FDO Secrets API.
 * This code was originally generated by qdbusxml2cpp v0.8. Suitable modifications have been
 * made to suit the needs of DFL::Keyring.
 **/

#include "Service.hpp"

/*
 * Implementation of interface class Secrets::Service
 */

Secrets::Service::Service( QObject *parent )
    : QDBusAbstractInterface( SecretsService, SecretsObject, staticInterfaceName(), QDBusConnection::sessionBus(), parent ) {
}


Secrets::Service::~Service() {
}


QList<QDBusObjectPath> Secrets::Service::collections() const {
    return qvariant_cast<QList<QDBusObjectPath> >( property( "Collections" ) );
}


QDBusReply<QDBusObjectPath> Secrets::Service::CreateCollection( const QVariantMap& properties, const QString& alias, QDBusObjectPath& prompt ) {
    QList<QVariant> argumentList;

    argumentList << QVariant::fromValue( properties ) << QVariant::fromValue( alias );
    QDBusMessage reply = callWithArgumentList( QDBus::Block, QStringLiteral( "CreateCollection" ), argumentList );

    if ( (reply.type() == QDBusMessage::ReplyMessage) && (reply.arguments().size() == 2) ) {
        prompt = qdbus_cast<QDBusObjectPath>( reply.arguments().at( 1 ) );
    }

    return reply;
}


QDBusReply<SecretsMap> Secrets::Service::GetSecrets( const QList<QDBusObjectPath>& items, const QDBusObjectPath& session ) {
    QList<QVariant> argumentList;

    argumentList << QVariant::fromValue( items ) << QVariant::fromValue( session );
    return callWithArgumentList( QDBus::Block, QStringLiteral( "GetSecrets" ), argumentList );
}


QDBusReply<QList<QDBusObjectPath> > Secrets::Service::Lock( const QList<QDBusObjectPath>& objects, QDBusObjectPath& Prompt ) {
    QList<QVariant> argumentList;

    argumentList << QVariant::fromValue( objects );
    QDBusMessage reply = callWithArgumentList( QDBus::Block, QStringLiteral( "Lock" ), argumentList );

    if ( (reply.type() == QDBusMessage::ReplyMessage) && (reply.arguments().size() == 2) ) {
        Prompt = qdbus_cast<QDBusObjectPath>( reply.arguments().at( 1 ) );
    }

    return reply;
}


QDBusReply<QDBusVariant> Secrets::Service::OpenSession( const QString& algorithm, const QDBusVariant& input, QDBusObjectPath& result ) {
    QList<QVariant> argumentList;

    argumentList << QVariant::fromValue( algorithm ) << QVariant::fromValue( input );
    QDBusMessage reply = callWithArgumentList( QDBus::Block, QStringLiteral( "OpenSession" ), argumentList );

    if ( (reply.type() == QDBusMessage::ReplyMessage) && (reply.arguments().size() == 2) ) {
        result = qdbus_cast<QDBusObjectPath>( reply.arguments().at( 1 ) );
    }

    return reply;
}


QDBusReply<QDBusObjectPath> Secrets::Service::ReadAlias( const QString& name ) {
    QList<QVariant> argumentList;

    argumentList << QVariant::fromValue( name );
    return callWithArgumentList( QDBus::Block, QStringLiteral( "ReadAlias" ), argumentList );
}


QDBusReply<QList<QDBusObjectPath> > Secrets::Service::SearchItems( StrStrMap attributes, QList<QDBusObjectPath>& locked ) {
    QList<QVariant> argumentList;

    argumentList << QVariant::fromValue( attributes );
    QDBusMessage reply = callWithArgumentList( QDBus::Block, QStringLiteral( "SearchItems" ), argumentList );

    if ( (reply.type() == QDBusMessage::ReplyMessage) && (reply.arguments().size() == 2) ) {
        locked = qdbus_cast<QList<QDBusObjectPath> >( reply.arguments().at( 1 ) );
    }

    return reply;
}


void Secrets::Service::SetAlias( const QString& name, const QDBusObjectPath& collection ) {
    QList<QVariant> argumentList;

    argumentList << QVariant::fromValue( name ) << QVariant::fromValue( collection );
    callWithArgumentList( QDBus::Block, QStringLiteral( "SetAlias" ), argumentList );
}


QDBusReply<QList<QDBusObjectPath> > Secrets::Service::Unlock( const QList<QDBusObjectPath>& objects, QDBusObjectPath& prompt ) {
    QList<QVariant> argumentList;

    argumentList << QVariant::fromValue( objects );
    QDBusMessage reply = callWithArgumentList( QDBus::Block, QStringLiteral( "Unlock" ), argumentList );

    if ( (reply.type() == QDBusMessage::ReplyMessage) && (reply.arguments().size() == 2) ) {
        prompt = qdbus_cast<QDBusObjectPath>( reply.arguments().at( 1 ) );
    }

    return reply;
}
