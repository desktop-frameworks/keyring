/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * The DFL::Impl::InbuiltKeyring is an implementation class that provides access to LXQt Wallet.
 **/

#pragma once
#include "DFKeyring.hpp"

namespace DFL {
    namespace Impl {
        class LXQtKeyring;
    }
}

class DFL::Impl::LXQtKeyring : public DFL::Keyring {
    Q_OBJECT;

    public:
        LXQtKeyring() = default;

        /**
         * Read the password for a key in a given wallet.
         * If the key is not found an empty QByteArray will be returned.
         */
        QByteArray retrieveEntry( QString, QString ) override {
            return QByteArray();
        }

        /**
         * Store the password for a key in a given wallet.
         * If the key already exists in a given wallet, it may be over-written.
         */
        bool createEntry( QString, QByteArray, QString, bool ) override {
            return false;
        }

        /**
         * Delete the key and the corresponding password from the given wallet.
         * If the key is not found, nothing is is done.
         */
        bool deleteEntry( QString, QString ) override {
            return false;
        }

        /**
         * Change the wallet password.
         * This may open a dedicated GUI.
         */
        bool changeWalletPassword( QString ) override {
            return false;
        }

        /**
         * Check if the given wallet is open.
         * Will return false if the wallet does not exist.
         */
        bool isOpen( QString ) override {
            return false;
        }

        /**
         * List all the available wallets in the given backend.
         * Will return 'Collections' ofr fdo-secrets, wallets for KWallet.
         */
        QStringList wallets() override {
            return QStringList();
        }

        /** List all the keys of a given wallet */
        QStringList keys( QString ) override {
            return QStringList();
        }

        /**
         * Check if this wallet is available or not.
         * This backend is current unavailable.
         */
        static bool isAvailable() {
            return false;
        }
};
