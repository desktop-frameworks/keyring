/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * The file contains implementation for various DBus types required by FDO Secrets API.
 **/

#include "Types.hpp"

QDataStream &operator<<( QDataStream& stream, const QDBusObjectPath& value ) {
    return stream << value.path();
}


QDataStream &operator>>( QDataStream& stream, QDBusObjectPath& value ) {
    QString str;

    stream >> str;
    value = QDBusObjectPath( str );
    return stream;
}


const QDBusArgument &operator>>( const QDBusArgument& arg, PropertiesMap& value ) {
    arg.beginMap();
    value.map.clear();

    while ( !arg.atEnd() ) {
        arg.beginMapEntry();
        QString  key;
        QVariant val;
        arg >> key >> val;

        /* For org.freedesktop.Secret.Item.Attributes */
        if ( val.canConvert<QDBusArgument>() ) {
            auto      metaArg = val.value<QDBusArgument>();
            StrStrMap metaMap;
            metaArg >> metaMap;
            val = QVariant::fromValue( metaMap );
        }

        value.map.insert( key, val );

        arg.endMapEntry();
    }
    arg.endMap();

    return arg;
}


QDBusArgument &operator<<( QDBusArgument& arg, const PropertiesMap& value ) {
    arg << value.map;
    return arg;
}


QDBusArgument &operator<<( QDBusArgument& arg, const Secret& secret ) {
    arg.beginStructure();
    arg << secret.session;
    arg << secret.parameters;
    arg << secret.value;
    arg << secret.mimeType;
    arg.endStructure();
    return arg;
}


const QDBusArgument &operator>>( const QDBusArgument& arg, Secret& secret ) {
    arg.beginStructure();
    arg >> secret.session;
    arg >> secret.parameters;
    arg >> secret.value;
    arg >> secret.mimeType;
    arg.endStructure();
    return arg;
}
