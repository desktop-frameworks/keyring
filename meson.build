project(
    'DFL Keyring',
    'c',
	'cpp',
	version: '0.2.0',
	license: 'GPLv3',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

add_project_arguments( '-DPROJECT_VERSION="v@0@"'.format( meson.project_version() ), language : 'cpp' )
add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

if get_option('use_qt_version') == 'qt5'
	Qt = import( 'qt5' )
	QtCore    = dependency( 'qt5', modules: [ 'Core' ] )
    QtDBus    = dependency( 'qt5', modules: [ 'DBus' ] )

	libname = 'df5keyring'
    subdirname = 'DFL/DF5'

elif get_option('use_qt_version') == 'qt6'
	Qt = import( 'qt6' )
	QtCore    = dependency( 'qt6', modules: [ 'Core' ] )
    QtDBus    = dependency( 'qt6', modules: [ 'DBus' ] )

	libname = 'df6keyring'
    subdirname = 'DFL/DF6'

endif

Deps = [ QtCore, QtDBus ]

Includes = include_directories( '.', 'Secrets' )

subdir( 'Secrets' )

Headers = [
    'DFKeyring.hpp',
    'DummyKeyring.hpp',
    'KWalletKeyring.hpp',
    'LXQtKeyring.hpp',
    'InbuiltKeyring.hpp',
    'SecretsKeyring.hpp',
]

Sources = [
    'Keyring.cpp',
    'KWalletKeyring.cpp',
    'LXQtKeyring.cpp',
    'InbuiltKeyring.cpp',
    'SecretsKeyring.cpp',
]

Mocs = Qt.compile_moc(
    headers : Headers,
    dependencies: Deps
)

keyring = shared_library(
    libname, [ Sources, Mocs ],
    version: meson.project_version(),
    dependencies: Deps,
    include_directories: Includes,
    link_with: secrets,
    install: true,
    install_dir: get_option( 'libdir' )
)

install_headers( [ 'DFKeyring.hpp' ], subdir: subdirname )

## PkgConfig Section
pkgconfig = import( 'pkgconfig' )
pkgconfig.generate(
	keyring,
	name: libname,
	subdirs: subdirname,
	version: meson.project_version(),
	filebase: libname,
	description: 'DFL::Keyring provides an abstract class for accessing various keyrings',
)
